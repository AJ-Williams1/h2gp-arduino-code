import pandas as pd
import serial
import os
import matplotlib
import matplotlib.pyplot as plt
import pylab

port = input("Enter port: ")

ser = serial.Serial(port)

# WHAT'S SENT: Time(ms),voltage(V),current(mA),coulomb-count(mA*s),packet#
dataColumns = {"Time (s)": [], "Volts (V)": [],
               "Current (mA)": [], "Couloumb Count (mA * H)": []}
df = pd.DataFrame(dataColumns)

plt.ion()
df.plot(x="Time (s)", y="Volts (V)")
plt.pause(0.0001)
plt.show()

print("Looking For RF")

counter = 0
while True:
    line = ser.readline().decode('ascii').strip()
    if line == "Running":
        continue

    print(line)

    if line:
        data = str(line).split(",")

        # Unit Fixes
        data[0] = float(data[0]) / 1000
        data[3] = float(data[3]) / 3600

        df.loc[data[len(data) - 1]] = data[:len(data) - 1]

        print(f"Packet Count: {data[len(data) - 1]}")
        for i, column in enumerate(df):
            print(f"{column}: {data[i]}\n")

    if (counter % 10 == 0):
        plt.close()
        df.plot(x="Time (s)", y="Volts (V)")
        plt.pause(0.0001)
        plt.show()

    if (counter % 50 == 0):
        df.to_excel(os.path.join(os.getcwd(), "Car Data.xlsx"))

    counter += 1
