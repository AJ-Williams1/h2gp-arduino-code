#include <RH_ASK.h>
#include <SPI.h>

RH_ASK driver;

struct dataStruct{
  float volts;
  float amps;
  unsigned long time;
  float coulumbCount;
  int counter;
}myData;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Running");
  driver.init();
}

int lastCount = 0;
void loop() {
  // put your main code here, to run repeatedly:
  uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
  uint8_t buflen = sizeof(buf); 
  if (driver.recv((uint8_t*)&buf, &buflen))
  {
    memcpy(&myData, buf, sizeof(myData));

    /*driver.printBuffer("Got:", buf, buflen);

        Serial.print("Time (ms): ");
    Serial.println(myData.time);

        Serial.print("Volts (V): ");
    Serial.println(myData.volts);

        Serial.print("Current (mA): ");
    Serial.println(myData.amps);

        Serial.print("Couloumb Count (mA * s): ");
    Serial.println(myData.coulumbCount);

    Serial.print("counter: ");
    Serial.println(myData.counter);

    if (lastCount + 1 != myData.counter)
    {
      Serial.println("***************        Packet Lost!!!!!!!!!    ********************");
    }
    lastCount = myData.counter;
    */

    Serial.print(myData.time); Serial.print(",");
    Serial.print(myData.volts); Serial.print(",");
    Serial.print(myData.amps); Serial.print(",");
    Serial.print(myData.coulumbCount); Serial.print(",");
    Serial.print(myData.counter);
    Serial.println();
  }
}

