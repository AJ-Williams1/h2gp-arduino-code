#include "Adafruit_INA219.h"
#include <RH_ASK.h>
#include <SPI.h>

Adafruit_INA219 ina219;
RH_ASK driver;

struct dataStruct{
  float volts;
  float amps;
  unsigned long time;
  float coulumbCount;
  int counter;
}myData;

byte tx_buf[sizeof(myData)] = {0};

void setup()
{
  pinMode(13, OUTPUT);

  //Serial.begin(9600);   //Commented for performance on transmitter

  driver.init();

  if (!ina219.begin()) {
    digitalWrite(13, HIGH);
  }

  myData.volts = 1;
  myData.amps = 2;
  myData.time = 0;
  myData.coulumbCount = 0;
  myData.counter = 0;
}


unsigned long lastTime = 0;
void loop()
{
  if (lastTime == 0)
    lastTime = millis();

  float shuntVoltage = 0;
  float busVoltage = 0;
  float current_mA = 0;
  float loadVoltage = 0;
  float power_mW = 0;
  unsigned long curTime = millis();
 
  shuntVoltage = ina219.getShuntVoltage_mV();
  busVoltage = ina219.getBusVoltage_V();
  current_mA = ina219.getCurrent_mA();
  power_mW = ina219.getPower_mW();
  loadVoltage = busVoltage + (shuntVoltage / 1000);

/*
  Serial.print("Bus Voltage:   "); Serial.print(busVoltage); Serial.println(" V");
  Serial.print("Shunt Voltage: "); Serial.print(shuntVoltage); Serial.println(" mV");
  Serial.print("Load Voltage:  "); Serial.print(loadVoltage); Serial.println(" V");
  Serial.print("Current:       "); Serial.print(current_mA); Serial.println(" mA");
  Serial.print("Power:         "); Serial.print(power_mW); Serial.println(" mW");
  Serial.println("");
*/

  myData.amps = current_mA;
  myData.volts = loadVoltage;
  myData.coulumbCount += current_mA * (curTime - lastTime) / 1000;
  myData.time = curTime;
  
  memcpy(tx_buf, &myData, sizeof(myData));
  byte zize=sizeof(myData);

  driver.send((uint8_t *)tx_buf, zize);
  driver.waitPacketSent();

  lastTime = curTime;
  myData.counter += 1;
  delay(200);
}
